from SCons.Script import *
import re

def handout(target, source, env):
	target = str(target[0])

	src = str(source[0].get_path(dir=source[0].get_dir()))
	src = re.sub(r'\.tex$', '', src)

	f = open(target, "w")
	f.write("\PassOptionsToClass{handout}{beamer}\n")
	f.write("\input{" + src + "}\n")
	f.close()
	return None

def solution(target, source, env):
	target = str(target[0])

	src = str(source[0].get_path(dir=source[0].get_dir()))
	src = re.sub(r'\.tex$', '', src)

	f = open(target, "w")
	f.write("\PassOptionsToClass{answers}{exam}\n")
	f.write("\input{" + src + "}\n")
	f.close()
	return None


def generate(env):
	env.Append(BUILDERS = {'Handout' : Builder(action = handout)})
	env.Append(BUILDERS = {'Solution' : Builder(action = solution)})

	bld = Builder(action = 'pdfnup --twoside --nup 2x2 --scale 0.9 --delta \'5mm 2mm\' --offset \'0mm -3mm\' --frame true --column true --outfile $TARGET $SOURCE')
	env.Append(BUILDERS = {'PDFnup2x2' : bld})

	bld = Builder(action = 'pdfnup --no-landscape --twoside --nup 1x2 --scale 0.9 --delta \'5mm 2mm\' --offset \'0mm -5mm\' --frame true --column true --outfile $TARGET $SOURCE')
	env.Append(BUILDERS = {'PDFnup1x2' : bld})

def exists(env):
	return True
